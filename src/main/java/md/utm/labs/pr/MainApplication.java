package md.utm.labs.pr;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import md.utm.labs.pr.controller.MainController;

public class MainApplication extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setController(MainController.getInstance());
		loader.load(getClass().getResourceAsStream("/layout.fxml"));
		Parent parent = loader.getRoot();
		primaryStage.setScene(new Scene(parent));
		primaryStage.setTitle("Web crawler v1.0");
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
