package md.utm.labs.pr.mediator;

import java.util.List;
import java.util.Map;

import md.utm.labs.pr.controller.MainController;
import md.utm.labs.pr.extractor.Link;

public interface WebCrawlerMediator {
	String getTargetUrl();
	void setHtml(String html);
	void setHttpStatus(int httpStatus);
	void setHttpHeaders(Map<String, List<String>> httpHeaders);
	String getCrawlerBaseUrl();
	void addCrawlerLink(Link link);
	void clearLinks();
	String getRequestMethod();
	String getRequestParameters();
	String getContentType();
	int getCrawlerDepth();
	String getPhraseToSearch();
	
	public static WebCrawlerMediator getMediator() {
		return MainController.getInstance();
	}
}
