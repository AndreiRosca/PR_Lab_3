package md.utm.labs.pr.extractor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class LinkTree {
	private final LinkNode startingLink;
	
	public LinkTree(Link link) {
		startingLink = new LinkNode(null, link);
	}
	
	public void addChild(Link link) {
		if (startingLink.getLink().getHref().equals(link.getParentLink()))
			startingLink.addChild(new LinkNode(startingLink, link));
		else {
			LinkNode parent = getParentNode(startingLink, link.getParentLink());
			if (parent != null) {
				parent.addChild(new LinkNode(parent, link));
			} else {
				System.err.println("Orphan node: " + link);
			}
		}
	}
	
	public int getDistanceFromRoot(Link leaf) {
		LinkNode parent = getParentNode(startingLink, leaf.getParentLink());
		int distance = 0;
		while (parent != null) {
			parent = parent.getParent();
			++distance;
		}
		return distance;
	}

	private LinkNode getParentNode(LinkNode parent, String parentToFind) {
		LinkedList<LinkNode> stack = new LinkedList<>();
		stack.addLast(parent);
		while (!stack.isEmpty()) {
			LinkNode node = stack.removeLast();
			if (node.getLink().getHref().equals(parentToFind))
				return node;
			else
				stack.addAll(node.getChildNodes());
		}
		return null;
	}
	
	public int getHeight() {
		return getHeight(startingLink) - 1;
	}

	private int getHeight(LinkNode parent) {
		int maxHeight = 0;
		for (LinkNode node : parent.getChildNodes()) {
			int height = getHeight(node);
			if (height > maxHeight)
				maxHeight = height;
		}
		return maxHeight + 1;
	}
	
	private static class LinkNode {
		private final LinkNode parent;
		private final Link link;
		private final Set<LinkNode> childNodes = new HashSet<>();
		
		public LinkNode(LinkNode parent, Link link) {
			this.parent = parent;
			this.link = link;
		}
		
		public LinkNode getParent() {
			return parent;
		}
		
		public Link getLink() {
			return link;
		}
		
		public boolean addChild(LinkNode node) {
			return childNodes.add(node);
		}
		
		public Set<LinkNode> getChildNodes() {
			return childNodes;
		}
	}
}
