package md.utm.labs.pr.extractor;

public class Link implements java.io.Serializable, Cloneable, Comparable<Link> {
	private static final long serialVersionUID = 1L;
	
	private final String href;
	private final String parentLink;
	private final int httpStatus;
	private final int searchedPhraseFrequency;
	private int linkNumber;

	public Link(String href, String parentLink, int httpStatus, int searchedPhraseFrequency) {
		this.href = href;
		this.parentLink = parentLink;
		this.httpStatus = httpStatus;
		this.searchedPhraseFrequency = searchedPhraseFrequency;
	}

	public String getHref() {
		return href;
	}
	
	public String getParentLink() {
		return parentLink;
	}

	public int getHttpStatus() {
		return httpStatus;
	}
	
	public int getLinkNumber() {
		return linkNumber;
	}
	
	public void setLinkNumber(int linkNumber) {
		this.linkNumber = linkNumber;
	}
	
	public boolean isAlive() {
		return httpStatus < 400;
	}
	
	public int getSearchedPhraseFrequency() {
		return searchedPhraseFrequency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((href == null) ? 0 : href.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Link other = (Link) obj;
		if (href == null) {
			if (other.href != null)
				return false;
		} else if (!href.equals(other.href))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Link{href=%s, httpStatus=%s}", href, httpStatus);
	}
	
	public Link clone() {
		try {
			return (Link) super.clone();
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	@Override
	public int compareTo(Link other) {
		return href.compareTo(other.href);
	}
}
