package md.utm.labs.pr.extractor;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LinkExtractor implements Runnable {
	private final List<Observer> observers = Collections.synchronizedList(new ArrayList<>());
	private final URL url;
	private final ExecutorService service;
	private final String searchedPhrase;
	
	public LinkExtractor(String location, ExecutorService service, String searchedPhrase) {
		this.service = service;
		this.searchedPhrase = searchedPhrase;
		try {
			url = new URL(location);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void registerObserver(Observer o) {
		observers.add(o);
	}
	
	private void pushLinkToObservers(Link link) {
		List<Observer> snapshot;
		synchronized (observers) {
			snapshot = new ArrayList<>(observers);
		}
		for (Observer o : snapshot) {
			o.consumeLink(link);
		}
	}
	
	public void run() {
		try {
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(false);
			InputStream stream = connection.getInputStream();
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
				String html = reader.lines().collect(Collectors.joining());
				Pattern p = Pattern.compile("href=\"(.*?)\"");
				Matcher m = p.matcher(html);
				while (m.find()) {
					String anchor = m.group(1);
					try {
						anchor = makeAnchorAbsolute(anchor);
						checkLinkLiveness(anchor, url.toString());
					} catch (IllegalArgumentException e) {
						System.err.println(e.getMessage());
					}
				}
			}
			connection.disconnect();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	private int findSearchedPhraseFrequency(String html) {
		int numberOfOccureneces = 0;
		int index;
		int fromIndex = 0;
		while ((index = html.indexOf(searchedPhrase, fromIndex)) >= 0) {
			++numberOfOccureneces;
			fromIndex = index + 1;
		}
		return numberOfOccureneces;
	}

	private String makeAnchorAbsolute(String anchor) {
		if (anchor.startsWith("/"))
			return url.toString() + anchor;
		else if (anchor.startsWith("http") || anchor.startsWith("ftp") || anchor.startsWith("www."))
			return anchor;
		else if (Character.isAlphabetic(anchor.charAt(0)))
			return url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "/" + anchor;
		throw new IllegalArgumentException(anchor + " does not have a valid protocol.");
	}

	public void extract() {
		service.submit(this);
	}
	
	private void checkLinkLiveness(String link, String parent) {
		service.submit(new LinkLivenessChecker(link, parent));
	}
	
	private class LinkLivenessChecker implements Runnable {
		private static final int TEN_SECONDS = 1000 * 10;
		
		private final String link;
		private final String parentLink;
		
		public LinkLivenessChecker(String link, String parentLink) {
			this.link = link;
			this.parentLink = parentLink;
		}
		
		public void run() {
			try {
				URL url = new URL(link);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(false);
				connection.setRequestMethod("GET");
				connection.setConnectTimeout(TEN_SECONDS);
				connection.connect();
				//connection.getInputStream().close();
				int statusCode = connection.getResponseCode();
				InputStream stream = connection.getInputStream();
				int searchedPhraseFrequency = 0;
				if (!"".equals(searchedPhrase)) {
					try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
						String html = reader.lines().collect(Collectors.joining());
						searchedPhraseFrequency = findSearchedPhraseFrequency(html);
					}					
				}
				connection.disconnect();
				Link l = new Link(link, parentLink, statusCode, searchedPhraseFrequency);
				pushLinkToObservers(l);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}
}
