package md.utm.labs.pr.extractor;

public interface Observer {

	void consumeLink(Link link);
}
