package md.utm.labs.pr.crawler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import md.utm.labs.pr.extractor.Link;
import md.utm.labs.pr.extractor.LinkExtractor;
import md.utm.labs.pr.extractor.LinkTree;
import md.utm.labs.pr.extractor.Observer;

public class WebCrawler implements Observer {
	private static final int MAX_THREADS = 100;
	
	private final String startingUrl;
	private final int maxDepth;
	private final Set<Link> visitedLinks = Collections.synchronizedSet(new HashSet<>());
	private final List<LinkObserver> observers = Collections.synchronizedList(new ArrayList<>());
	private final LinkTree linkTree;
	private final ExecutorService service;
	private final String searchedPhrase;

	public WebCrawler(String startingUrl, int maxDepth, String searchedPhrase) {
		this.startingUrl = startingUrl;
		this.maxDepth = maxDepth;
		this.searchedPhrase = searchedPhrase;
		service = Executors.newFixedThreadPool(MAX_THREADS, new DaemonThreadFactory());
		linkTree = new LinkTree(new Link(startingUrl, null, 200, 0));
	}
	
	public void crawl() {
		LinkExtractor extractor = new LinkExtractor(startingUrl, service, searchedPhrase);
		extractor.registerObserver(this);
		extractor.extract();
	}
	
	public void registerLinkObserver(LinkObserver o) {
		observers.add(o);
	}

	@Override
	public synchronized void consumeLink(Link link) {
		boolean linkWasntVisited = visitedLinks.add(link);
		boolean maxDepthNotReached;
		synchronized (linkTree) {
			linkTree.addChild(link);
			maxDepthNotReached = linkTree.getDistanceFromRoot(link) < maxDepth;
		}
		if (maxDepthNotReached) {
			LinkExtractor extractor = new LinkExtractor(link.getHref(), service, searchedPhrase);
			extractor.registerObserver(this);
			extractor.extract();	
		}			
		if (linkWasntVisited)
			publishLink(link);
	}

	private void publishLink(Link link) {
		List<LinkObserver> snapshot;
		synchronized (observers) {
			snapshot = new ArrayList<>(observers);
		}
		for (LinkObserver o : snapshot)
			o.consumeLink(link);
	}

	public void dispose() {
		service.shutdownNow();
	}
	
	private static class DaemonThreadFactory implements ThreadFactory {

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		}
	}
}
