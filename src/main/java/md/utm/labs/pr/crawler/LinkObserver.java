package md.utm.labs.pr.crawler;

import md.utm.labs.pr.extractor.Link;

public interface LinkObserver {
	void consumeLink(Link link);
}
