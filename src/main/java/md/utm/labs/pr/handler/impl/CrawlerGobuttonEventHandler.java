package md.utm.labs.pr.handler.impl;

import md.utm.labs.pr.crawler.LinkObserver;
import md.utm.labs.pr.crawler.WebCrawler;
import md.utm.labs.pr.extractor.Link;
import md.utm.labs.pr.handler.BaseMenuEventHandler;

public class CrawlerGobuttonEventHandler extends BaseMenuEventHandler implements LinkObserver {
	private WebCrawler crawler;

	public CrawlerGobuttonEventHandler() {
		super("crawlerGoButton");
	}

	@Override
	public void handle() {
		getMediator().clearLinks();
		String url = getMediator().getCrawlerBaseUrl();
		int depth = getMediator().getCrawlerDepth();
		String searchedPhrase = getMediator().getPhraseToSearch();
		if (crawler != null)
			crawler.dispose();
		crawler = new WebCrawler(url, depth, searchedPhrase);
		crawler.registerLinkObserver(this);
		crawler.crawl();
	}

	@Override
	public void consumeLink(Link link) {
		getMediator().addCrawlerLink(link);
	}
}
