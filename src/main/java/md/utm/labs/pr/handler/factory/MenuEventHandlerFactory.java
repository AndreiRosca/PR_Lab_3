package md.utm.labs.pr.handler.factory;

import md.utm.labs.pr.handler.impl.*;

public class MenuEventHandlerFactory {

	public static void makeHandlers() {
		new HttpClientGoButtonEventHandler();
		new CrawlerGobuttonEventHandler();
	}
}
