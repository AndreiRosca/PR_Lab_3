package md.utm.labs.pr.handler;

import md.utm.labs.pr.mediator.WebCrawlerMediator;

public interface MenuEventHandler {
	void handle();
	
	public default WebCrawlerMediator getMediator() {
		return WebCrawlerMediator.getMediator();
	}
}
