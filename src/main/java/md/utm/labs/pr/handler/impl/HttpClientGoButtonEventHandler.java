package md.utm.labs.pr.handler.impl;

import md.utm.labs.pr.handler.BaseMenuEventHandler;
import md.utm.labs.pr.http.HttpClient;

public class HttpClientGoButtonEventHandler extends BaseMenuEventHandler {

	private static final int TEN_SECONDS = 1000 * 10;

	public HttpClientGoButtonEventHandler() {
		super("goButtonId");
	}

	@Override
	public void handle() {
		String method = getMediator().getRequestMethod();
		String paremeters = getMediator().getRequestParameters();
		String contentType = getMediator().getContentType();
		String url = getMediator().getTargetUrl();
		HttpClient client = HttpClient.newBuilder()
				.setUrl(url)
				.setMethod(method)
				.setContentType(contentType)
				.setParameters(paremeters)
				.build();
		client.executeRequest()
			  .thenAccept(result -> {
				  getMediator().setHtml(result.getBody());
				  getMediator().setHttpStatus(result.getStatus());
				  getMediator().setHttpHeaders(result.getHttpHeaders());
		});
	}
}
