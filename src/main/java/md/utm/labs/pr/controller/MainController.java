package md.utm.labs.pr.controller;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import md.utm.labs.pr.extractor.Link;
import md.utm.labs.pr.handler.MenuEventHandlerRegistry;
import md.utm.labs.pr.handler.factory.MenuEventHandlerFactory;
import md.utm.labs.pr.mediator.WebCrawlerMediator;

public class MainController implements Initializable, WebCrawlerMediator {
	private static final Map<Integer, PropertyValueFactory<Link, String>> columnFactories 
		= new HashMap<>();
	private static final MainController instance = new MainController();
	private static final MenuEventHandlerRegistry eventHandlerRegistry = 
			MenuEventHandlerRegistry.getInstance();

	@FXML
	private MenuBar menuBar;
	
	@FXML
	private Button goButtonId;

	@FXML
	private TextField urlTextField;
	
	@FXML 
	private WebView htmlWebWiew;
	
	@FXML
	private ChoiceBox<String> httpMethodsChoiceBox;
	
	@FXML
	private ListView<String> httpHeadersListView;
	
	@FXML 
	private TextField requestParametersTextField;
	
	@FXML
	private TextField contentTypeTextField;
	
	//
	@FXML
	private TextField crawlerUrlTextField;
	
	@FXML 
	private Button crawlerGoButton;
	
	@FXML
	private TableView<Link> crawlerLinkTableView;
	
	@FXML
	private TextField crawlerDepthTextField;
	
	@FXML 
	private TextField searchPhraseTextField;
	
	private final AtomicInteger linkCounter = new AtomicInteger();
	
	//@FXML
	//private TextField validLinkStatusTextField;
	
	static {
		columnFactories.put(0, new PropertyValueFactory<>("linkNumber"));
		columnFactories.put(1, new PropertyValueFactory<>("href"));
		columnFactories.put(2, new PropertyValueFactory<>("httpStatus"));
		columnFactories.put(3, new PropertyValueFactory<>("searchedPhraseFrequency"));
	}
	
	private MainController() {
	}

	public static MainController getInstance() {
		return instance;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		MenuEventHandlerFactory.makeHandlers();
		registerMenuHandlers();
		goButtonId.setOnAction(eventHandlerRegistry);
		ObservableList<String> httpMethods = FXCollections.observableArrayList("GET", "POST");
		httpMethodsChoiceBox.setItems(httpMethods);
		httpMethodsChoiceBox.setValue(httpMethods.get(0));
		
		crawlerGoButton.setOnAction(eventHandlerRegistry);
		
		int i = 0;
		for (TableColumn column : crawlerLinkTableView.getColumns()) {
			column.setCellValueFactory(columnFactories.get(i++));
		}
	}

	private void registerMenuHandlers() {
		for (Menu m : menuBar.getMenus())
			for (MenuItem item : m.getItems())
				item.setOnAction(eventHandlerRegistry);
	}

	@Override
	public String getTargetUrl() {
		return urlTextField.getText();
	}

	@Override
	public void setHtml(String html) {
		Platform.runLater(() -> {
			WebEngine engine = htmlWebWiew.getEngine();
			engine.loadContent(html);			
		});
	}

	@Override
	public void setHttpStatus(int httpStatus) {
		System.out.println("Status: " + httpStatus);
	}

	@Override
	public void setHttpHeaders(Map<String, List<String>> httpHeaders) {
		ObservableList<String> headerList = FXCollections.observableArrayList();
		for (Map.Entry<String, List<String>> entry : httpHeaders.entrySet()) {
			String headerValue = entry.getValue().stream().collect(Collectors.joining(";"));
			headerList.add(entry.getKey() + " -> " + headerValue);
		}
		Platform.runLater(() -> {
			httpHeadersListView.setItems(headerList);			
		});
	}

	@Override
	public String getCrawlerBaseUrl() {
		return crawlerUrlTextField.getText();
	}

	@Override
	public void addCrawlerLink(Link link) {
		System.out.println(link);
		Platform.runLater(() -> {
			link.setLinkNumber(linkCounter.incrementAndGet());
			crawlerLinkTableView.getItems().add(link);
			crawlerLinkTableView.scrollTo(link);	
		});
	}

	@Override
	public void clearLinks() {
		linkCounter.set(0);
		crawlerLinkTableView.getItems().clear();
	}

	@Override
	public String getRequestMethod() {
		return httpMethodsChoiceBox.getValue();
	}

	@Override
	public String getRequestParameters() {
		return requestParametersTextField.getText();
	}

	@Override
	public String getContentType() {
		return contentTypeTextField.getText();
	}

	@Override
	public int getCrawlerDepth() {
		return Integer.valueOf(crawlerDepthTextField.getText());
	}

	@Override
	public String getPhraseToSearch() {
		return searchPhraseTextField.getText();
	}
}
