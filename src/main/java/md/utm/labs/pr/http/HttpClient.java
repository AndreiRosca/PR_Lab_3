package md.utm.labs.pr.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class HttpClient {
	private static final int TEN_SECONDS = 1000 * 10;
	
	private final String method;
	private final String parameters;
	private final String contentType;
	private final String url;
	
	private HttpClient(String url, String method, String parameters, String contentType) {
		this.url = url;
		this.method = method;
		this.parameters = parameters;
		this.contentType = contentType;
	}
	
	public CompletableFuture<HttpRequestResult> executeRequest() {
		CompletableFuture<HttpRequestResult> future = new CompletableFuture<>();
		new Thread(() -> makeRequest(future)).start();
		return future;
	}
	
	private void makeRequest(CompletableFuture<HttpRequestResult> future) {
		String location = url;
		try {
			URL url = new URL(location);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(method);
			if (method.equalsIgnoreCase("POST"))
				connection.setDoOutput(true);
			else 
				connection.setDoOutput(false);
			connection.setConnectTimeout(TEN_SECONDS);
			connection.setRequestProperty("Content-Type", contentType);
			connection.connect();
			if (method.equalsIgnoreCase("POST"))
				writePostBody(parameters, contentType, connection);
			extractRequestResult(connection, future);
			connection.disconnect();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void writePostBody(String parameters, String contentType, HttpURLConnection connection)
			throws IOException, UnsupportedEncodingException {
		OutputStream out = connection.getOutputStream();
		String encodedParams = null;
		if (contentType.equals("application/x-www-form-urlencoded"))
			encodedParams = URLEncoder.encode(parameters, "UTF-8");
		else
			encodedParams = parameters;
		out.write(encodedParams.getBytes());
	}
	
	private void extractRequestResult(HttpURLConnection connection, 
			CompletableFuture<HttpRequestResult> future) {
		HttpRequestResult result = getRequestResult(connection);
		future.complete(result);
	}
	
	private HttpRequestResult getRequestResult(HttpURLConnection connection) {
		try {
			InputStream stream = connection.getInputStream();
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
				String html = reader.lines().collect(Collectors.joining());
				Map<String, List<String>> httpHeaders = connection.getHeaderFields();
				int httpStatus = connection.getResponseCode();
				return new HttpRequestResult(httpStatus, html, httpHeaders);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static HttpClientBuilder newBuilder() {
		return new HttpClientBuilder();
	}
	
	public static class HttpClientBuilder {
		private String url;
		private String method;
		private String parameters;
		private String contentType;
		
		public HttpClientBuilder setUrl(String url) {
			this.url = url;
			return this;
		}
		
		public HttpClientBuilder setMethod(String method) {
			this.method = method;
			return this;
		}
		
		public HttpClientBuilder setParameters(String parameters) {
			this.parameters = parameters;
			return this;
		}
		
		public HttpClientBuilder setContentType(String contentType) {
			this.contentType = contentType;
			return this;
		}
		
		public HttpClient build() {
			return new HttpClient(url, method, parameters, contentType);
		}
	}
}
