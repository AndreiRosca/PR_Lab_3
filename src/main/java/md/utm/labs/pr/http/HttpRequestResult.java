package md.utm.labs.pr.http;

import java.util.List;
import java.util.Map;

public class HttpRequestResult {
	private final int status;
	private final String body;
	private final Map<String, List<String>> httpHeaders;

	public HttpRequestResult(int status, String body, Map<String, List<String>> httpHeaders) {
		this.status = status;
		this.body = body;
		this.httpHeaders = httpHeaders;
	}

	public int getStatus() {
		return status;
	}

	public String getBody() {
		return body;
	}

	public Map<String, List<String>> getHttpHeaders() {
		return httpHeaders;
	}
}
