package md.utm.labs.pr.extractor;

import static org.junit.Assert.*;

import org.junit.Test;

public class LinkTreeTest {
	
	private LinkTree tree = new LinkTree(new Link("http://localhost:8080/index.jsp", null, 200, 0));

	@Test
	public void treeWithRootOnlyHashHeightEqualToZero() {
		assertEquals(0, tree.getHeight());
	}
	
	@Test
	public void treeWithOneChildHashHeightEqualToOne() {
		Link link = new Link("http://localhost:8080/a.jsp", 
				"http://localhost:8080/index.jsp", 200, 0);
		tree.addChild(link);
		assertEquals(1, tree.getHeight());
	}
	
	@Test
	public void treeWithTwoChildHashHeightEqualToTwo() {
		Link first = new Link("http://localhost:8080/a.jsp", 
				"http://localhost:8080/index.jsp", 200, 0);
		Link second = new Link("http://localhost:8080/b.jsp", 
				"http://localhost:8080/index.jsp", 200, 0);
		tree.addChild(first);
		tree.addChild(second);
		assertEquals(1, tree.getHeight());
	}
	
	@Test
	public void test() {
		Link first = new Link("http://localhost:8080/a.jsp", 
				"http://localhost:8080/index.jsp", 200, 0);
		Link second = new Link("http://localhost:8080/b.jsp", 
				"http://localhost:8080/a.jsp", 200, 0);
		tree.addChild(first);
		tree.addChild(second);
		assertEquals(2, tree.getHeight());
	}
}
